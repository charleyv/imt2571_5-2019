<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */

    public function getActorStatistics()
    {
        $result = array();

        //  Gets the all actors
        $allActors = $this->xpath->query("/Disney/Actors/Actor");
        //  Iterates over all the actors and creates an associative array
        foreach ($allActors as $actor) {    //  Retrieves the actor's name
          $actorName = $actor->childNodes[1]->firstChild->nodeValue;
          $actorId = $actor->getAttribute('id');   // Attains the actor's id to identify roles
                                      //  Creates an array which associates roles
          $result[$actorName] = array();
                                        // Finds the movies where the actorId is present
          $movies = $this->xpath->query("/Disney/Subsidiaries/".
          "Subsidiary/Movie[Cast/Role[@actor ='$actorId']]");

          foreach ($movies as $movie) {
                                      // Finds the movie name and year
            $movieName = $movie->childNodes[1]->firstChild->nodeValue;
            $movieYear = $movie->childNodes[3]->firstChild->nodeValue;
                                      //  Query for the role name
            $roleName = $this->xpath->query("/Disney/Subsidiaries/Subsidiary/"
              . "Movie[Name='$movieName']/Cast/Role[@actor='$actorId']");
                                      //  Nodevalue of the role node
            $roleName = $roleName->item(0)->getAttribute('name');
                                      //  The string to be inserted in the array
            $roles = "As $roleName in $movieName ($movieYear)";
            array_push($result[$actorName], $roles);
          }

       }



       return $result;
  }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
        //To do:
        // Implement functionality as specified
        //  Gets the all actors which have no movies in a nodelist
        $actorsToBeRemoved = $this->xpath->query("/Disney/Actors/Actor[not(@id".
        " = //Disney/Subsidiaries/Subsidiary/Movie/Cast/Role/@actor)]");
        foreach ($actorsToBeRemoved as $actor){
          $actorParentNode = $actor->parentNode;
          $actorParentNode->removeChild($actor);
        }
    }
    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        //To do:
        // Implement functionality as specified
              // Query
        $query = "/Disney/Subsidiaries/Subsidiary[@id='$subsidiaryId']".
        "/Movie[Name='$movieName' and Year = $movieYear]/Cast";
              //  Create an element wiht the name 'Role'
        $role = $this->doc->createElement('Role');
              //  Query, set movieCast variable to be the nodeList
        $movieCast = $this->xpath->query($query)->item(0);

        $movieCast->appendChild($role);
        $role->setAttribute('name', $roleName);
        if($roleAlias!= null) {   // IF the role has an alias, set that attribute
          $role->setAttribute('alias', $roleAlias);
        }
        $role->setAttribute('actor', $roleActor);
    }
}
?>
